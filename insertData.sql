-- FICHIER DE TEST POUR TESTER SI LA BD FONCTIONNE COMME VOULU

insert into CREATEUR values(1, 'Cyprien', 'Touraine', NULL);

insert into CATEGORIE values(1, 'thriller');
insert into CATEGORIE values(2, 'sci-fi');

insert into PROJET values(1, 'À Coeur Ouvert', 'La famille brisée', 'roman', NULL, 'Cyprien Touraine', NULL, 1);
insert into PROJET values(2, 'S.E.L.M.A.', NULL, 'roman', NULL, 'Cyprien Touraine', NULL, 2);

insert into EST_CREE values(1, 1, '2020-10-01');
insert into EST_CREE values(1, 2, '2019-08-15');

insert into LIEU values(1, 'salle de bain', 'Montpellier', 'France', 'salle de bain dans laquelle un meutre a été commis');
insert into LIEU values(2, 'Conceicao', 'Conceicao', 'Eternera', 'ville désastrée par une catastrophe');

insert into EST_CONTENU values(1, 1);
insert into EST_CONTENU values(2, 2);

insert into PERSONNAGE values(1, 'Fargo', 'Alan', 'miaou', '2028-04-19', 'H', 5, 'Conceicao', NULL);
insert into PERSONNAGE values(2, 'Fargo', 'Cameron', 'Cam', '2003-05-13', 'H', 4, 'Conceicao', NULL);

insert into APPARTIENT values(1, 2);
insert into APPARTIENT values(2, 2);