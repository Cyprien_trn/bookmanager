drop table EST_CREE;
drop table CREATEUR;
drop table APPARTIENT;
drop table EST_CONTENU;
drop table CATEGORISE;
drop table PROJET;
drop table PERSONNAGE;
drop table PHYSIQUE_PERS;
drop table PROFIL_PERS;
drop table HISTOIRE_PERS;
drop table CARACT_PERS;
drop table CATEGORIE;
drop table LIEU;

create table CREATEUR(
    idCrea int,
    prenomCrea varchar(42),
    nomCrea varchar(42),
    descCrea varchar(42),
    PRIMARY KEY(idCrea)
);

create table EST_CREE(
    idCrea int,
    idPr int,
    PRIMARY KEY(idCrea, idPr)
);

create table PROJET(
    idPr int,
    titreP varchar(42),
    sousTitreP varchar(42),
    dateCreationP Date,
    typeP varchar(42),
    resumeP varchar(500),
    nomRealCouv varchar(42),
    imgCouv varchar(100),
    idCat int NOT NULL,
    PRIMARY KEY(idPr)
);

create table CATEGORIE(
    idCat int,
    intituleCat varchar(42),
    PRIMARY KEY(idCat)
);

create table CATEGORISE(
    idCat int,
    idPr int,
    PRIMARY KEY(idCat, idPr)
);

create table LIEU(
    idL int,
    nomL varchar(42),
    villeL varchar(42),
    paysL varchar(42),
    descL varchar(500),
    PRIMARY KEY(idL)
);

create table EST_CONTENU(
    idL int,
    idPr int,
    PRIMARY KEY(idL, idPr)
);

create table PERSONNAGE(
    idPers int,
    nomPers varchar(42),
    prenomPers varchar(42),
    surnomPers varchar(42),
    DdNPers Date,
    genrePers varchar(1),
    importanceRolePers int(1),
    villeResidePers varchar(42),
    photoPers varchar(100),
    idPhysiquePers int,
    idCaractPers int,
    idProfilPers int,
    idHistoirePers int,
    PRIMARY KEY(idPers)
);

create table APPARTIENT(
    idPers int,
    idPr int,
    PRIMARY KEY(idPers, idPr)
);

create table PHYSIQUE_PERS(
    idPhysiquePers int,
    taille int,
    poids int,
    descPhysique varchar(500),
    tenue varchar(500),
    particularitePhysique varchar(500),
    accessoireTypique varchar(250),
    PRIMARY KEY(idPhysiquePers)
);

create table CARACT_PERS(
    idCaractPers int,
    gouts varchar(500),
    qualites varchar(250),
    defauts varchar(250),
    doutes varchar(500),
    peurs varchar(500),
    intelligenceP int(2),
    forceP int(2),
    agiliteP int(2),
    perceptionP int(2),
    robustesseP int(2),
    sociabiliteP int(2),
    empathie int(2),
    PRIMARY KEY(idCaractPers)
);

create table PROFIL_PERS(
    idProfilPers int,
    education varchar(500),
    croyances varchar(500),
    secrets varchar(500),
    lieuxMarquants varchar(500),
    expressionTypique varchar(250),
    PRIMARY KEY(idProfilPers)
);

create table HISTOIRE_PERS(
    idHistoirePers int,
    but varchar(1000),
    passe varchar(1000),
    present varchar(1000),
    futur varchar(1000),
    PRIMARY KEY(idHistoirePers)
);


alter table EST_CREE add foreign key (idCrea) references CREATEUR(idCrea);
alter table EST_CREE add foreign key (idPr) references PROJET(idPr);
alter table EST_CONTENU add foreign key (idL) references LIEU(idL);
alter table EST_CONTENU add foreign key (idPr) references PROJET(idPr);
alter table APPARTIENT add foreign key (idPers) references PERSONNAGE(idPers);
alter table APPARTIENT add foreign key (idPr) references PROJET(idPr);
alter table CATEGORISE add foreign key (idPr) references PROJET(idPr);
alter table CATEGORISE add foreign key (idCat) references CATEGORIE(idCat);
alter table PERSONNAGE add foreign key (idPhysiquePers) references PHYSIQUE_PERS(idPhysiquePers);
alter table PERSONNAGE add foreign key (idCaractPers) references CARACT_PERS(idCaractPers);
alter table PERSONNAGE add foreign key (idHistoirePers) references HISTOIRE_PERS(idHistoirePers);
alter table PERSONNAGE add foreign key (idProfilPers) references PROFIL_PERS(idProfilPers);