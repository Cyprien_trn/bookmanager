source creation.sql;

-- On charge les créateurs
LOAD DATA LOCAL INFILE 'datas/createur.csv' INTO TABLE CREATEUR 
FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\n' IGNORE 1 LINES 
(@v1,@v2,@v3,@v4) set idCrea=NULLIF(@v1,''), prenomCrea=NULLIF(@v2,''), nomCrea=NULLIF(@v3,''), descCrea=NULLIF(@v4,'');

-- On charge les catégories
LOAD DATA LOCAL INFILE 'datas/categorie.csv' INTO TABLE CATEGORIE 
FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\n' IGNORE 1 LINES 
(@v1,@v2) set idCat=NULLIF(@v1,''), intituleCat=NULLIF(@v2,'');

-- On charge les projets :
LOAD DATA LOCAL INFILE 'datas/projet.csv' INTO TABLE PROJET 
FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\n' IGNORE 1 LINES 
(@v1,@v2,@v3,@v4,@v5,@v6,@v7,@v8,@v9) set idPr=NULLIF(@v1,''), titreP=NULLIF(@v2,''), sousTitreP=NULLIF(@v3,''), dateCreationP=NULLIF(@v4, ''),
typeP=NULLIF(@v5,''), resumeP=NULLIF(@v6,''), nomRealCouv=NULLIF(@v7,''), imgCouv=NULLIF(@v8,''), idCat=NULLIF(@v9,'');

-- On charge l'association entre PROJET et CREATEUR :
LOAD DATA LOCAL INFILE 'datas/est_cree.csv' INTO TABLE EST_CREE 
FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\n' IGNORE 1 LINES 
(@v1,@v2) set idCrea=NULLIF(@v1,''), idPr=NULLIF(@v2,'');

-- On charge l'association entre PROJET et CATEGORIE :
LOAD DATA LOCAL INFILE 'datas/categorise.csv' INTO TABLE CATEGORISE 
FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\n' IGNORE 1 LINES 
(@v1,@v2) set idCat=NULLIF(@v1,''), idPr=NULLIF(@v2,'');

-- On charge les lieux :
LOAD DATA LOCAL INFILE 'datas/lieu.csv' INTO TABLE LIEU 
FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\n' IGNORE 1 LINES 
(@v1,@v2,@v3,@v4,@v5) set idL=NULLIF(@v1,''), nomL=NULLIF(@v2,''), villeL=NULLIF(@v3,''), paysL=NULLIF(@v4,''), descL=NULLIF(@v5,'');
/*
-- On charge les personnages :
LOAD DATA LOCAL INFILE 'datas/personnage.csv' INTO TABLE PERSONNAGE 
FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\n' IGNORE 1 LINES 
(@v1,@v2,@v3,@v4,@v5,@v6,@v7,@v8,@v9,@v10,@v11,@v12,@v13) set idPers=NULLIF(@v1, ''), nomPers=NULLIF(@v2, ''), prenomPers=NULLIF(@v3, ''),
surnomPers=NULLIF(@v4, ''), DdNPers=NULLIF(@v5, ''), genrePers=NULLIF(@v6, ''), importanceRolePers=NULLIF(@v7, ''), villeResidePers=NULLIF(@v8, ''),
photoPers=NULLIF(@v9, ''), idPhysiquePers=NULLIF(@v10, ''), idCaractPers=NULLIF(@v11, ''), idProfilPers=NULLIF(@v12, ''), idHistoirePers=NULLIF(@v13, '');

-- On charge l'association entre PROJET et PERSONNAGE :
LOAD DATA LOCAL INFILE 'datas/appartient.csv' INTO TABLE APPARTIENT 
FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\n' IGNORE 1 LINES 
(@v1,@v2) set idPers=NULLIF(@v1,''), idPr=NULLIF(@v2,'');

-- On charge les caractéristiques physiques des personnages :
LOAD DATA LOCAL INFILE 'datas/physique_pers.csv' INTO TABLE PHYSIQUE_PERS 
FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\n' IGNORE 1 LINES 
(@v1,@v2,@v3,@v4,@v5,@v6,@v7) set idPhysiquePers=NULLIF(@v1, ''), taille=NULLIF(@v2, ''), poids=NULLIF(@v3, ''), descPhysique=NULLIF(@v4, ''),
tenue=NULLIF(@v5, ''), particularitePhysique=NULLIF(@v6, ''), accessoireTypique=NULLIF(@v7, '');

-- On charge les caractères des personnages :
LOAD DATA LOCAL INFILE 'datas/caract_pers.csv' INTO TABLE CARACT_PERS 
FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\n' IGNORE 1 LINES 
(@v1,@v2,@v3,@v4,@v5,@v6,@v7,@v8,@v9,@v10,@v11,@v12,@v13) set idCaractPers=NULLIF(@v1, ''), gouts=NULLIF(@v2, ''), qualites=NULLIF(@v3, ''),
defauts=NULLIF(@v4, ''), doutes=NULLIF(@v5, ''), peurs=NULLIF(@v6, ''), intelligenceP=NULLIF(@v7, ''), forceP=NULLIF(@v8, ''),
agiliteP=NULLIF(@v9, ''), perceptionP=NULLIF(@v10, ''), robustesseP=NULLIF(@v11, ''), sociabiliteP=NULLIF(@v12, ''), empathie=NULLIF(@v13, '');

-- On charge les profils des personnages :
LOAD DATA LOCAL INFILE 'datas/profil_pers.csv' INTO TABLE PROFIL_PERS 
FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\n' IGNORE 1 LINES 
(@v1,@v2,@v3,@v4,@v5,@v6) set idProfilPers=NULLIF(@v1, ''), education=NULLIF(@v2, ''), croyances=NULLIF(@v3, ''), secrets=NULLIF(@v4, ''),
lieuxMarquants=NULLIF(@v5, ''), expressionTypique=NULLIF(@v6, '');

-- On charge les histoires des personnages :
LOAD DATA LOCAL INFILE 'datas/physique_pers.csv' INTO TABLE PHYSIQUE_PERS 
FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\n' IGNORE 1 LINES 
(@v1,@v2,@v3,@v4,@v5,@v6,@v7) set idHistoirePers=NULLIF(@v1, ''), but=NULLIF(@v2, ''), passe=NULLIF(@v3, ''),
present=NULLIF(@v4, ''), futur=NULLIF(@v5, '');*/